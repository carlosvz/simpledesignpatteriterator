﻿using System.Collections;
using System.Collections.Generic;

namespace SimpleDesignPatternIterator
{
    public class ConcreteAggregate : IEnumerable
    {
        private List<Client> _client = new List<Client>();

        public IEnumerator GetEnumerator()
        {
            return new ConcreteIterator(this);
        }

        public Client this[int index]
        {
            get { return _client[index]; }
        }

        public int Count
        {
            get { return _client.Count; }
        }

        public void Add(Client client)
        {
            _client.Add(client);
        }
    }
}
