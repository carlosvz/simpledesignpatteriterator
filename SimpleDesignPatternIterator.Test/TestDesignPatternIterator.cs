﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleDesignPatternIterator.Test
{
    [TestClass]
    public class TestDesignPatternIterator
    {
        [TestMethod]
        [TestCategory("Sample iterator")]
        public void ExecuteSampleIterator()
        {
            var concreteAggregate = InitializeNewConcreteAggregate();
            var iterator = concreteAggregate.GetEnumerator();
            iterator.Reset();
            
            while (iterator.MoveNext())
            {
                var itemCurrent = (Client)iterator.Current;

            }
            
        }

        private static ConcreteAggregate InitializeNewConcreteAggregate()
        {
            var concreteAggregate = new ConcreteAggregate();

            concreteAggregate.Add(new Client { Id = 1, Nome = "Carlos" });
            concreteAggregate.Add(new Client { Id = 2, Nome = "Moises" });
            concreteAggregate.Add(new Client { Id = 3, Nome = "Fabio" });
            concreteAggregate.Add(new Client { Id = 4, Nome = "Leandro" });
            concreteAggregate.Add(new Client { Id = 5, Nome = "Herlan" });

            return concreteAggregate;
        }
    }
}
